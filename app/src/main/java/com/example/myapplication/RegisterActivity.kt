package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegisterActivity : AppCompatActivity() {

    private lateinit var editTextEmail:EditText
    private lateinit var editTextPassword:EditText
    private lateinit var editTextRepeatPassword:EditText
    private lateinit var buttonRegister: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
        registerListeners()
    }


    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextRepeatPassword = findViewById(R.id.editTextRepeatPassword)
        buttonRegister = findViewById(R.id.buttonRegister)
    }


    private fun registerListeners() {
        buttonRegister.setOnClickListener {
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val repeatpassword = editTextRepeatPassword.text.toString()

            if (email.isEmpty() || password.isEmpty() || repeatpassword.isEmpty()) {
                Toast.makeText(this, "E-mail or passwords are empty", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            else if (validpassword(password,repeatpassword) && validemail(email)) {

                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
                    .addOnCompleteListener { task ->
                        if(task.isSuccessful) {
                            startActivity(Intent(this, LoginActivity::class.java))
                            finish()
                        }else {
                            Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
                        }
                    }
            }
        }
    }


    private fun validpassword(password:String, repeatpassword:String):Boolean{

        if(password.length < 9) {
            Toast.makeText(this, "password should be 9 charachers or more ", Toast.LENGTH_SHORT).show()
            return false
        }
        if(!password.matches(".*[0-9].*".toRegex())) {
            Toast.makeText(this, "password should contain at last 1 digit", Toast.LENGTH_SHORT).show()
            return false
        }
        if(!password.matches(".*[A-Z].*".toRegex())) {
            Toast.makeText(this, "password should contain an upper-case lettert at least once", Toast.LENGTH_SHORT).show()
            return false
        }
        if(!password.matches(".*[a-z].*".toRegex())) {
            Toast.makeText(this, "password should contain an lower-case letter at least once", Toast.LENGTH_SHORT).show()
            return false
        }
        if (!password.matches(".*[!@#$%^&*+=/?].*".toRegex())) {
            Toast.makeText(this, "password should contain 1 symbol", Toast.LENGTH_SHORT).show()
            return false
        }
        if (password != repeatpassword) {
            Toast.makeText(this, "password and repeat password should be same", Toast.LENGTH_SHORT).show()
        }
        return true

    }
    private fun validemail(email:String):Boolean {
        if(Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return true
        }
        Toast.makeText(this, "E-mail is not valid", Toast.LENGTH_SHORT).show()
        return false
    }

}
