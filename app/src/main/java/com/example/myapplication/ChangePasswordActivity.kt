package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class ChangePasswordActivity : AppCompatActivity() {
    private lateinit var editTextNewPassword:EditText
    private lateinit var buttonNewPassword:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        init()
        registerListeners()
    }
    private fun init() {
        editTextNewPassword = findViewById(R.id.editTextNewPassword)
        buttonNewPassword = findViewById(R.id.buttonNewPassword)

    }
    private fun registerListeners() {
        buttonNewPassword.setOnClickListener {

            val newPassword = editTextNewPassword.text.toString()

            if(newPassword.isEmpty() || newPassword.length < 6) {
                Toast.makeText(this, "Incorrect new password ", Toast.LENGTH_SHORT).show()
            }
            else if(validpassword(newPassword)) {
                FirebaseAuth.getInstance().currentUser?.updatePassword(newPassword)
                    ?.addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(this, "Success!", Toast.LENGTH_SHORT).show()
                            gotoProfile()
                        } else {
                            Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
                        }
                    }
            }
        }
    }
    private fun gotoProfile() {
        startActivity(Intent(this, ProfileActivity::class.java))
        finish()
    }
    private fun validpassword(password:String):Boolean{

        if(password.length < 9) {
            Toast.makeText(this, "password should be 9 charachers or more ", Toast.LENGTH_SHORT).show()
            return false
        }
        if(!password.matches(".*[0-9].*".toRegex())) {
            Toast.makeText(this, "password should contain at last 1 digit", Toast.LENGTH_SHORT).show()
            return false
        }
        if(!password.matches(".*[A-Z].*".toRegex())) {
            Toast.makeText(this, "password should contain an upper-case letter  at least once", Toast.LENGTH_SHORT).show()
            return false
        }
        if(!password.matches(".*[a-z].*".toRegex())) {
            Toast.makeText(this, "password should contain an lower-case letter at least once", Toast.LENGTH_SHORT).show()
            return false
        }
        if (!password.matches(".*[!@#$%^&*+=/?].*".toRegex())) {
            Toast.makeText(this, "password should contain 1 symbol", Toast.LENGTH_SHORT).show()
            return false
        }

        return true

    }
}