package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class PasswordResetActivity : AppCompatActivity() {
    private lateinit var editTextEmail:EditText
    private lateinit var buttonSendEmail:Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_reset)
        init()
        registerListeners()
    }
    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmail)
        buttonSendEmail = findViewById(R.id.buttonSendEmail)
    }
    private fun registerListeners() {
        buttonSendEmail.setOnClickListener {
            val email = editTextEmail.text.toString()
            if(email.isEmpty()){
                Toast.makeText(this, "please input your email", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (validemail(email)){

                FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful){
                            Toast.makeText(this, "Check email!", Toast.LENGTH_SHORT).show()
                        }
                        else {
                            Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
                        }
                    }
            }

        }

    }
    private fun validemail(email:String):Boolean {
        if(Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return true
        }
        Toast.makeText(this, "E-mail is not valid", Toast.LENGTH_SHORT).show()
        return false
    }
}